/*!
 * Game 2048 script
 */
(function($) {

    //add two numbers to start the game
    addNumber();
    addNumber();
    //addNumber();

    function addNumber() {
        var emptyBox = $('.empty').length;
        console.log('empty boxes ' + emptyBox);
        if (emptyBox == 0) {
            alert('Game over');
        }
        var randomBox = Math.floor(Math.random() * emptyBox);
        var newNumberBox = $('.empty').eq(randomBox);
        $(newNumberBox).removeClass('empty').addClass('full').append('<div class="innerBox"><span>2</span></div>');

        //alert(randomBox);
    }


    //key down test
    $('html').keydown(function(e) {
        switch (event.keyCode) {
            case 39:
                //right pressed

                $('.full').sort(function(a, b) {
                    var attrA = $(a).attr('colmn');
                    var attrB = $(b).attr('colmn');
                    if (attrA > attrB) {
                        return -1;
                    }
                    if (attrA < attrB) {
                        return 1;
                    }
                    return 0;
                }).each(function() {

                    var thisCol = parseInt($(this).attr('colmn'));
                    var thisRow = parseInt($(this).attr('row'));
                    var thisNumber = parseInt($(this).children().text());
                    var lastEmptyCol = 0;
                    var emptyMe = false;
                    var compared = false;

                    for (i = thisCol + 1; i <= 3; i++) {

                        if ($('.box[colmn="' + i + '"][row="' + thisRow + '"]').hasClass('full')) {
                            //check if this number already compared
                            if ($('.box[colmn="' + i + '"][row="' + thisRow + '"]').hasClass('compared')) {
                                //it's already compared, leave it alone	
                            } else {
                                //get number
                                //if number equals, merge numbers

                                var tempNumb = parseInt($('.box[colmn="' + i + '"][row="' + thisRow + '"]').children().text());

                                if (thisNumber == tempNumb) {
                                    tempNumb = tempNumb + tempNumb;
                                    //destroy number in current box
                                    //set new number
                                    $('.box[colmn="' + i + '"][row="' + thisRow + '"]').find('span').text(tempNumb);
                                    $('.box[colmn="' + i + '"][row="' + thisRow + '"]').find('.innerBox').removeClass()
                                        .addClass('innerBox').addClass('number' + tempNumb);

                                    emptyMe = true;
                                    compared = true;

                                }
                                $('.box[colmn="' + i + '"][row="' + thisRow + '"]').addClass('compared');
                            }


                        } else {
                            lastEmptyCol = i;
                            //we need to move number to empty box 
                            $(this).addClass('moveMe');

                        }
                    }
                    //move the box with number
                    if (emptyMe) {
                        $(this).empty().removeClass('full').removeClass('moveMe').addClass('empty');
                    }
                    if ($(this).hasClass('moveMe')) {
                        $(this).attr('newCol', lastEmptyCol).attr('newRow', thisRow);

                    }

                    moveTheBoxes();
                });

                break;

            case 37:
                //left pressed

                $('.full').sort(function(a, b) {
                    var attrA = $(a).attr('colmn');
                    var attrB = $(b).attr('colmn');
                    if (attrA > attrB) {
                        return 1;
                    }
                    if (attrA < attrB) {
                        return -1;
                    }
                    return 0;
                }).each(function() {

                    var thisCol = parseInt($(this).attr('colmn'));
                    var thisRow = parseInt($(this).attr('row'));
                    var thisNumber = parseInt($(this).children().text());
                    var lastEmptyCol = 0;
                    var emptyMe = false;
                    var compared = false;

                    for (i = thisCol - 1; i >= 0; i--) {

                        if ($('.box[colmn="' + i + '"][row="' + thisRow + '"]').hasClass('full')) {
                            //check if this number already compared
                            if ($('.box[colmn="' + i + '"][row="' + thisRow + '"]').hasClass('compared')) {
                                //it's already compared, leave it alone
                            } else {
                                //get number
                                //if number equals, merge numbers

                                var tempNumb = parseInt($('.box[colmn="' + i + '"][row="' + thisRow + '"]').children().text());
                                if (thisNumber == tempNumb) {
                                    tempNumb = tempNumb + tempNumb;
                                    //destroy number in current box
                                    //set new number
                                    $('.box[colmn="' + i + '"][row="' + thisRow + '"]').find('span').text(tempNumb);
                                    $('.box[colmn="' + i + '"][row="' + thisRow + '"]').find('.innerBox').removeClass()
                                        .addClass('innerBox').addClass('number' + tempNumb);

                                    emptyMe = true;
                                    compared = true;

                                }
                                $('.box[colmn="' + i + '"][row="' + thisRow + '"]').addClass('compared');
                            }


                        } else {
                            lastEmptyCol = i;
                            //we need to move number to empty box 
                            $(this).addClass('moveMe');

                        }
                    }
                    //move the box with number
                    if (emptyMe) {
                        $(this).empty().removeClass('full').removeClass('moveMe').addClass('empty');
                    }
                    if ($(this).hasClass('moveMe')) {
                        $(this).attr('newCol', lastEmptyCol).attr('newRow', thisRow);

                    }

                    moveTheBoxes();
                });

                break;

            case 38:
                //up pressed

                $('.full').sort(function(a, b) {
                    var attrA = $(a).attr('row');
                    var attrB = $(b).attr('row');
                    if (attrA > attrB) {
                        return 1;
                    }
                    if (attrA < attrB) {
                        return -1;
                    }
                    return 0;
                }).each(function() {
                    //alert($(this).attr('row'));
                    var thisCol = parseInt($(this).attr('colmn'));
                    var thisRow = parseInt($(this).attr('row'));
                    var thisNumber = parseInt($(this).children().text());
                    var lastEmptyRow = 0;
                    var emptyMe = false;
                    var compared = false;

                    for (i = thisRow - 1; i >= 0; i--) {

                        if ($('.box[colmn="' + thisCol + '"][row="' + i + '"]').hasClass('full')) {
                            //check if this number already compared
                            if ($('.box[colmn="' + thisCol + '"][row="' + i + '"]').hasClass('compared')) {
                                //it's already compared, leave it alone
                                //alert('leave compared alone');
                            } else {
                                //get number
                                //if number equals, merge numbers

                                var tempNumb = parseInt($('.box[colmn="' + thisCol + '"][row="' + i + '"]').children().text());
                                if (thisNumber == tempNumb) {
                                    tempNumb = tempNumb + tempNumb;
                                    //destroy number in current box
                                    //set new number
                                    $('.box[colmn="' + thisCol + '"][row="' + i + '"]').find('span').text(tempNumb);
                                    $('.box[colmn="' + thisCol + '"][row="' + i + '"]').find('.innerBox').removeClass()
                                        .addClass('innerBox').addClass('number' + tempNumb);

                                    emptyMe = true;
                                    compared = true;

                                }
                                $('.box[colmn="' + thisCol + '"][row="' + i + '"]').addClass('compared');
                            }


                        } else {
                            lastEmptyRow = i;
                            //we need to move number to empty box 
                            $(this).addClass('moveMe');

                        }
                    }
                    //move the box with number
                    if (emptyMe) {
                        $(this).empty().removeClass('full').removeClass('moveMe').addClass('empty');
                    }
                    if ($(this).hasClass('moveMe')) {
                        $(this).attr('newRow', lastEmptyRow).attr('newCol', thisCol);

                    }

                    moveTheBoxes();
                });

                break;

            case 40:
                //down pressed

                $('.full').sort(function(a, b) {
                    var attrA = $(a).attr('row');
                    var attrB = $(b).attr('row');
                    if (attrA > attrB) {
                        return -1;
                    }
                    if (attrA < attrB) {
                        return 1;
                    }
                    return 0;
                }).each(function() {

                    var thisCol = parseInt($(this).attr('colmn'));
                    var thisRow = parseInt($(this).attr('row'));
                    var thisNumber = parseInt($(this).children().text());
                    var lastEmptyRow = 0;
                    var emptyMe = false;
                    var numberBetween = false;

                    for (i = thisRow + 1; i <= 3; i++) {

                        if ($('.box[colmn="' + thisCol + '"][row="' + i + '"]').hasClass('full')) {
                            //check if this number already compared with neighbour
                            if ($('.box[colmn="' + thisCol + '"][row="' + i + '"]').hasClass('compared')) {
                                //it's already compared, leave it alone
                            } else {
                                //get number
                                //if number equals, merge numbers

                                var tempNumb = parseInt($('.box[colmn="' + thisCol + '"][row="' + i + '"]').children().text());

                                if (thisNumber == tempNumb) {
                                    tempNumb = tempNumb + tempNumb;
                                    //destroy number in current box
                                    //set new number
                                    $('.box[colmn="' + thisCol + '"][row="' + i + '"]').find('span').text(tempNumb);
                                    $('.box[colmn="' + thisCol + '"][row="' + i + '"]').find('.innerBox').removeClass()
                                        .addClass('innerBox').addClass('number' + tempNumb);
                                    emptyMe = true;

                                }
                                $('.box[colmn="' + thisCol + '"][row="' + i + '"]').addClass('compared');
                            }


                        } else {
                            lastEmptyRow = i;
                            //we need to move number to empty box 
                            $(this).addClass('moveMe');

                        }
                    }
                    //move the box with number
                    if (emptyMe) {
                        $(this).empty().removeClass('full').removeClass('moveMe').addClass('empty');
                    }
                    if ($(this).hasClass('moveMe')) {
                        $(this).attr('newRow', lastEmptyRow).attr('newCol', thisCol);

                    }

                    moveTheBoxes();
                });

                break;

        }

        cleanClasses();
        addNumber();
    });

    var moveTheBoxes = function() {

        $('.moveMe').each(function() {

            var newCol = $(this).attr('newcol');
            var newRow = $(this).attr('newrow');
            if (newCol > -1 && newRow > -1) {
                $(this).children(".innerBox").detach().appendTo('.box[colmn="' + newCol + '"][row="' + newRow + '"]');
                //change classes for new box with number
                $('.box[colmn="' + newCol + '"][row="' + newRow + '"]').addClass('full').removeClass('empty');

                //clean classes and attributes
                $(this).removeClass('full').removeClass('moveMe').addClass('empty');
                $(this).attr('newcol', -1);
                $(this).attr('newrow', -1);
            }
        });

    };

    var cleanClasses = function() {
        $('.box').removeClass('compared');
    };



})(jQuery);